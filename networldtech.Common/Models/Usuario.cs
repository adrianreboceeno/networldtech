﻿namespace networldtech.Common.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Usuario
    {
        [Key]
        public int UsuarioId { get; set; }

        [Required(ErrorMessage = "Debes ingresar tu nombre")]
        [Display(Name ="Nombre(s)")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Debes ingresar tu apellido paterno")]
        [Display(Name = "Apellido paterno")]
        public string ApellidoPaterno { get; set; }

        [Required(ErrorMessage = "Debes ingresar tu apellido materno")]
        [Display(Name = "Apellido materno")]
        public string ApellidoMaterno { get; set; }

        [Required(ErrorMessage = "Debes ingresar tu número de teléfono")]
        [Display(Name = "Número de Teléfono")]
        public string NumeroTelefono { get; set; }

        [Required(ErrorMessage = "Debes ingresar tu número de teléfono")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo electronico")]
        public string CorreoElectronico { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha Registro")]
        public DateTime FechaRegistro { get; set; }

        public override string ToString()
        {
            return this.Nombre;
        }
    }
}
