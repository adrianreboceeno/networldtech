﻿namespace networldtech.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using Common.Models;
    using GalaSoft.MvvmLight.Command;
    using Services;
    using Xamarin.Forms;

    public class UsuariosViewModel : BaseViewModel
    {
        #region Attributes
        private ApiService apiService;

        private bool isRefreshing;

        private ObservableCollection<Usuario> usuarios;
        #endregion
        public ObservableCollection<Usuario> Usuarios
        {
            get { return this.usuarios; }
            set { this.SetValue(ref this.usuarios, value); }
        }

        #region Constructors
        public UsuariosViewModel()
        {
            this.apiService = new ApiService();
            this.LoadUsuarios();
        }
        #endregion

        #region Methods
        private async void LoadUsuarios()
        {
            IsRefreshing = true;

            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert("Error", connection.Message,  "Accept");
                return;
            }


            var url = Application.Current.Resources["UrlAPI"].ToString();
            var prefix = Application.Current.Resources["UrlPrefix"].ToString();
            var controller = Application.Current.Resources["UrlUsuariosController"].ToString();
            var response = await this.apiService.GetList<Usuario>(url, prefix, controller);
            if (!response.IsSuccess)
            {
                IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert("Error",response.Message,"Accept");
                return;
            }
            var list = (List<Usuario>)response.Result;
            this.Usuarios = new ObservableCollection<Usuario>(list);
            IsRefreshing = false;
        }
        #endregion

        #region Properties
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }
        #endregion

        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadUsuarios);
            }
        }
    }
}
