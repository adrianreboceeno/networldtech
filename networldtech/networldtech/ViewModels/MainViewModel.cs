﻿using System;
using System.Collections.Generic;
using System.Text;

namespace networldtech.ViewModels
{
    public class MainViewModel
    {
        //excepción Resultado por que solo es una sola vistas por lo general no se hacen instancias  de este tipo
        public UsuariosViewModel Usuarios { get; set; }
        public MainViewModel()
        {
            this.Usuarios = new UsuariosViewModel();
        }
    }
}
