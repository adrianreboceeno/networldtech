﻿

namespace networldtech.Backend.Models
{
    using networldtech.Domain.Models;

    public class LocalDataContext : DataContext
    {
        public System.Data.Entity.DbSet<networldtech.Common.Models.Usuario> Usuarios { get; set; }
    }
}