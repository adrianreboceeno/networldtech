﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(networldtech.Backend.Startup))]
namespace networldtech.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
